# ansible

## How to check facts using ansible (Not playbook)
ansible localhost -m ansible.builtin.setup

## Ansible playbook description
* check_facts.yml : facts 확인
* check_hostname.yml : hostname 추출
* check_ipaddress.yml : ip 추출(become: yes 일 경우 해당 변수 생성하지 않음)
* script_archive_fetch.yml : 원격서버에서 스크립트 실행 후 결과파일을 압축하여 가져오기
* push_exec_get.yml : 원격서버에서 스크립트 실행 후 결과파일을 특정 패턴으로 찾아 가져오고 원격지 결과파일은 삭제하기
* with_dict_sample : with_dict를 이용한 TCPWRAPPER 설정
